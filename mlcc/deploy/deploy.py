import argparse

import docker

parser = argparse.ArgumentParser()
parser.add_argument("--imageDir", required=True, help="Directory with docker image files")
args = parser.parse_args()

client = docker.from_env()
client.images.build(path=args.imageDir, tag='mlcc-test')
print(client.containers.run(image='mlcc-test', auto_remove=True).decode("utf-8"))
