import pkgutil
import yaml
from string import Template
from mlcc.common import ConfigUtil

from mlcc.template.task.base_python.deploy_gen import generate as base_python_deploy_generate

default_config_filename = "default-config.yaml"
docker_file = "Dockerfile"
additional_conda_config_filename = "conda.yaml"
additional_cmds_filename = "additional_cmds.txt"


def load_resource(name):
    return pkgutil.get_data("mlcc.template.task.mlcc_pipeline", name).decode("utf-8")


def generate(project_config, project_name, project_variant, project_version, task_name, deploy_config, output_dir):

    default_deploy_config = yaml.safe_load(load_resource(default_config_filename))
    additional_conda_config = yaml.safe_load(load_resource(additional_conda_config_filename))
    docker_resource = load_resource(docker_file)

    add_nvidia_key = ''
    task_deploy_config = ConfigUtil.get_config_value(config=project_config, path=['tasks', task_name, 'deploy'], default={})
    if task_deploy_config.get('gpu', False):
        add_nvidia_key = 'RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys A4B469963BF863CC'
        default_deploy_config["baseImageName"] = 'nvcr.io/nvidia/tensorrt:22.12-py3'

    docker_resource = Template(docker_resource).safe_substitute(
        **{'addNvidiaKey': add_nvidia_key})

    base_python_deploy_generate(
        project_config=project_config,
        project_name=project_name,
        project_variant=project_variant,
        project_version=project_version,
        task_name=task_name,
        deploy_config=deploy_config,
        output_dir=output_dir,
        default_deploy_config=default_deploy_config,
        additional_conda_config=additional_conda_config,
        docker_resource = docker_resource
    )
