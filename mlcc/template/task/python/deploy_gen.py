import pkgutil
import yaml

from mlcc.template.task.base_python.deploy_gen import generate as base_python_deploy_generate

docker_filename = "Dockerfile"
default_config_filename = "default-config.yaml"


def load_resource(name):
    return pkgutil.get_data("mlcc.template.task.python", name).decode("utf-8")


def generate(project_config, project_name, project_variant, project_version, task_name, deploy_config, output_dir):

    default_deploy_config = yaml.safe_load(load_resource(default_config_filename))

    base_python_deploy_generate(
        project_config=project_config,
        project_name=project_name,
        project_variant=project_variant,
        project_version=project_version,
        task_name=task_name,
        deploy_config=deploy_config,
        output_dir=output_dir,
        default_deploy_config=default_deploy_config,
    )
