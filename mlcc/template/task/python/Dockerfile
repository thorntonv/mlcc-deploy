FROM $baseImageName

# Install base utilities
RUN apt-get update && \
    apt-get install -y wget && \
    apt-get install -y ca-certificates && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Environment
ENV CONDA_DIR /opt/conda
ENV PATH=$$CONDA_DIR/bin:$$PATH
ENV MLCC_PROJECT_NAME=$projectName
ENV MLCC_PROJECT_VARIANT=$projectVariant
ENV MLCC_PROJECT_VERSION=$projectVersion
ENV MLCC_ML_REGISTRY_HOST=$mlRegistryHost
ENV MLCC_EVENT_BUS_HOST=$eventBusHost
$envCommands

# Install miniconda
RUN wget --quiet $miniCondaDownloadUrl -O ~/miniconda.sh && \
     /bin/bash ~/miniconda.sh -b -p /opt/conda && rm ~/miniconda.sh

WORKDIR /app

$copyPipConfig
#RUN conda install pip -n base
# Install pip packages
$pipCommands

# Update the conda environment
COPY $condaFile .
RUN conda env update -n base --file $condaFile \
    && conda clean -afy \
    && pip cache purge

$additionalCommands

# Copy files
$copyCommands

$entryPoint
