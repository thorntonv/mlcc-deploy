from setuptools import setup, find_namespace_packages

if __name__ == "__main__": setup(
    name='mlcc-deploy',
    packages=find_namespace_packages(include=['mlcc.*'])
)

