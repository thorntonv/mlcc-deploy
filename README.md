MLCC deployment

### Packaging

Follow these steps to build the project into a wheel:
- Install pypa-build, if it's not installed already (pip -m install build)
- Build the Python package with setuptools (python3 -m build --sdist --wheel .)

Now there should be a build directory and dist directory generated. For distribution to Artifactory, the two files in the dist directory should be used (the .tar.gz file and .whl files).
In a fresh environment to install the package, just run pip install dist/mlcc-python-common-1.0.0-py3-none-any.whl.