from glassdoor.vault.VaultClientBuilder import VaultClientBuilder
from glassdoor.vault.VaultPropertyStore import VaultPropertyStore

from common import common

print("Hello World!!")
common.say_hello()

client = VaultClientBuilder().buildVaultClient()
properties = VaultPropertyStore.load(client, VaultPropertyStore.VAULT_PROPERTIES_ROOT_PREFIX)
print(properties)

