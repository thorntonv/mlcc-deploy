import os

projectName = os.environ['MLCC_PROJECT_NAME']
projectVariant = os.environ['MLCC_PROJECT_VARIANT']

print(f'Hello {projectName}:{projectVariant}')